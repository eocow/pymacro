# planning for now
'''
links
https://pypi.org/project/evdev/ 
https://github.com/gvalkov/python-evdev
https://python-evdev.readthedocs.io/en/latest/usage.html#listing-accessible-event-devices
plus the reddit post by u/timeload on r/linux
'''



import os
from evdev import InputDevice, categorize, ecodes

dev = InputDevice("dev/input/event#")
dev.grab()

for event in dev.read_loop():
  if event.type == ecodes.EV_KEY:
    key = categorize(event)
  if key.keystate == key.key_down:
    if key.keycode == "KEY_ESC":
      os.system("echo Hello World")
    if key.keycode ==  "KEY_A":
      pass
      

# ripped from a reddit post by u/TimeLoad on r/Linux [link if i get the time]
# need to make a way to get the event id auto
# this is from https://python-evdev.readthedocs.io/en/latest/usage.html#listing-accessible-event-devices 

devices = [evdev.InputDevice(path) for path in evdev.list_devices()]
for device in devices:
  print(device.path, device.name, device.phys)
# if no work try in .../input for r/w perms. 
# output> /dev/input/event1    USB Keyboard        usb-0000:00:12.1-2/input0

# do this and ask the user which one? 
# maybe parse the event "#" and ask for that
# same as above for source 

for event in device.read_loop():
  if event.type == evdev.ecodes.EV_KEY:
    print(evdev.categorize(event))

# good for finding key code names
'''
need a way to send keystrokes but also to remove the funactionality of the keybaord 
maybe send a os.system command for xinput to remove it from the list, this is annoying though as it:
#1 removes wayland support
#2 maybe leads to keybaord being bricked if not re-enabled correctly
#3 could mean program needs sudo to work
but 
doing that means that i wont have the same issue lau macros has with games and raw input

wait nvm, i think .grab does what i need it to do. idk if csgo will respect it though so it may need the xinput job
> Grab input device using EVIOCGRAB - other applications will be unable to receive events until the device is released. Only one process can hold a EVIOCGRAB on a device.
'''

# i also want to make this a gui program using pysimplegui
# use the test.py as an example
# i will prob best make functions for each of the steps of the setup and macro procedure then call in the gui sections when needed
# for now macros will have to setup through the actual script but i can try and make a text doc and format that i can read from (like a scuffed config file)